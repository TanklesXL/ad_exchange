#!/usr/bin/bash
go build -o=dsp ./cmd/dsp
go build -o=exchange ./cmd/exchange

# run DSPs
./dsp -conf=./cmd/dsp/conf_1.yml &>>dsp_1.log &
DSP_1_PID=$!
./dsp -conf=./cmd/dsp/conf_2.yml &>>dsp_2.log &
DSP_2_PID=$!
./dsp -conf=./cmd/dsp/conf_3.yml &>>dsp_3.log &
DSP_3_PID=$!

echo "dsp 1: $DSP_1_PID"
echo "dsp 2: $DSP_2_PID"
echo "dsp 3: $DSP_3_PID"

./exchange -conf=./cmd/exchange/exchange.yml &>>exchange.log &
EXCHANGE_PID=$!
echo "exchange: $EXCHANGE_PID"

cd elm
elm reactor &
ELM=$!

# kill $DSP_1_PID
# kill $DSP_2_PID
# kill $DSP_3_PID
# kill $EXCHANGE_PID
# kill $ELM
# rm *.log
