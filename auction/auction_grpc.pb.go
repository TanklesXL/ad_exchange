// Code generated by protoc-gen-go-grpc. DO NOT EDIT.

package auction

import (
	context "context"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
)

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
// Requires gRPC-Go v1.32.0 or later.
const _ = grpc.SupportPackageIsVersion7

// DSPClient is the client API for DSP service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://pkg.go.dev/google.golang.org/grpc/?tab=doc#ClientConn.NewStream.
type DSPClient interface {
	Bid(ctx context.Context, in *BidReq, opts ...grpc.CallOption) (*BidResp, error)
	NotifyWin(ctx context.Context, in *WinAlert, opts ...grpc.CallOption) (*Empty, error)
}

type dSPClient struct {
	cc grpc.ClientConnInterface
}

func NewDSPClient(cc grpc.ClientConnInterface) DSPClient {
	return &dSPClient{cc}
}

func (c *dSPClient) Bid(ctx context.Context, in *BidReq, opts ...grpc.CallOption) (*BidResp, error) {
	out := new(BidResp)
	err := c.cc.Invoke(ctx, "/DSP/Bid", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *dSPClient) NotifyWin(ctx context.Context, in *WinAlert, opts ...grpc.CallOption) (*Empty, error) {
	out := new(Empty)
	err := c.cc.Invoke(ctx, "/DSP/NotifyWin", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// DSPServer is the server API for DSP service.
// All implementations must embed UnimplementedDSPServer
// for forward compatibility
type DSPServer interface {
	Bid(context.Context, *BidReq) (*BidResp, error)
	NotifyWin(context.Context, *WinAlert) (*Empty, error)
	mustEmbedUnimplementedDSPServer()
}

// UnimplementedDSPServer must be embedded to have forward compatible implementations.
type UnimplementedDSPServer struct {
}

func (UnimplementedDSPServer) Bid(context.Context, *BidReq) (*BidResp, error) {
	return nil, status.Errorf(codes.Unimplemented, "method Bid not implemented")
}
func (UnimplementedDSPServer) NotifyWin(context.Context, *WinAlert) (*Empty, error) {
	return nil, status.Errorf(codes.Unimplemented, "method NotifyWin not implemented")
}
func (UnimplementedDSPServer) mustEmbedUnimplementedDSPServer() {}

// UnsafeDSPServer may be embedded to opt out of forward compatibility for this service.
// Use of this interface is not recommended, as added methods to DSPServer will
// result in compilation errors.
type UnsafeDSPServer interface {
	mustEmbedUnimplementedDSPServer()
}

func RegisterDSPServer(s grpc.ServiceRegistrar, srv DSPServer) {
	s.RegisterService(&DSP_ServiceDesc, srv)
}

func _DSP_Bid_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(BidReq)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(DSPServer).Bid(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/DSP/Bid",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(DSPServer).Bid(ctx, req.(*BidReq))
	}
	return interceptor(ctx, in, info, handler)
}

func _DSP_NotifyWin_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(WinAlert)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(DSPServer).NotifyWin(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/DSP/NotifyWin",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(DSPServer).NotifyWin(ctx, req.(*WinAlert))
	}
	return interceptor(ctx, in, info, handler)
}

// DSP_ServiceDesc is the grpc.ServiceDesc for DSP service.
// It's only intended for direct use with grpc.RegisterService,
// and not to be introspected or modified (even as a copy)
var DSP_ServiceDesc = grpc.ServiceDesc{
	ServiceName: "DSP",
	HandlerType: (*DSPServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "Bid",
			Handler:    _DSP_Bid_Handler,
		},
		{
			MethodName: "NotifyWin",
			Handler:    _DSP_NotifyWin_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "auction/auction.proto",
}
