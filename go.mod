module gitlab.com/tanklesxl/ad_exchange

go 1.16

require (
	github.com/gin-contrib/cors v1.3.1
	github.com/gin-gonic/gin v1.7.1
	github.com/google/uuid v1.2.0
	github.com/spf13/viper v1.7.1
	google.golang.org/grpc v1.37.0
	google.golang.org/protobuf v1.26.0
)
