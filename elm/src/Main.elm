module Main exposing (main)

import Browser
import Delay exposing (after)
import Html exposing (div, img)
import Html.Attributes as Attributes
import Http exposing (expectString, jsonBody)
import Json.Encode as E


main : Program () Model Msg
main =
    Browser.document
        { init = init
        , view = view
        , update = update
        , subscriptions = \_ -> Sub.none
        }



-- MODEL


type Model
    = Default
    | External String
    | Timeout


type Msg
    = GetAd
    | GotAd (Result Http.Error String)


init : () -> ( Model, Cmd Msg )
init _ =
    ( Default, getAd )



-- UPDATE


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        GetAd ->
            ( model, getAd )

        GotAd result ->
            case result of
                Ok newAd ->
                    ( External newAd, Cmd.none )

                Err _ ->
                    ( Timeout, after 2 Delay.Second GetAd )


getAd : Cmd Msg
getAd =
    Http.post
        { url = "http://localhost:8080/image"
        , body = jsonBody (E.object [ ( "width", E.int 800 ), ( "height", E.int 600 ) ])
        , expect = expectString GotAd
        }



-- VIEW


view : Model -> Browser.Document Msg
view model =
    let
        source =
            getSourceFromModel model
    in
    { title = "Let's Get That Ad!"
    , body = [ div [] [ img [ Attributes.src source ] [] ] ]
    }


getSourceFromModel : Model -> String
getSourceFromModel model =
    case model of
        Default ->
            ""

        External newAd ->
            newAd

        Timeout ->
            "https://thumbs-prod.si-cdn.com/CjQWDFslASFJhvbDXOCdfjMFRtQ=/800x600/filters:no_upscale():focal(1204x1082:1205x1083)/https://public-media.si-cdn.com/filer/89/f2/89f21dea-e4f3-4d50-aba9-73f582e63ad8/09_30_2014_wyandotte_chicken.jpg"
