package main

import (
	"log"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	pb "gitlab.com/tanklesxl/ad_exchange/auction"
	"google.golang.org/grpc"
)

// adSpot holds the width and height of the spot to be filled.
type adSpot struct {
	Width  uint64 `json:"width"`
	Height uint64 `json:"height"`
}

// image responds to the image ad request by starting an auction and returning the winning bid.
func image(c *gin.Context) {
	var spot adSpot // decode details of the ad spot information
	if err := c.BindJSON(&spot); err != nil || spot.Height == 0 || spot.Width == 0 {
		c.AbortWithStatus(http.StatusBadRequest)
		return
	}

	// details for the auction
	auctionID := uuid.New().String()
	req := &pb.BidReq{AdType: pb.AdType_IMAGE, Width: spot.Width, Height: spot.Height, AuctionID: auctionID}

	conns := c.MustGet("CONNS").(map[string]*grpc.ClientConn) // get the connections to use
	timeout := c.MustGet("TIMEOUT").(time.Duration)           // get the timeout to apply for each request

	// broadcast bid request and retrieve all responses, fail if none were received
	bids := broadcastBidRequest(conns, req, timeout)
	if len(bids) == 0 {
		c.AbortWithStatus(http.StatusNotFound)
		return
	}

	// extract winning bid and price paid, log them
	address, winner := getWinnerAndPrice(bids)
	price := winner.GetBidAmount()
	log.Printf("Auction %s: winnner=%s, value=%d\n", auctionID, address, price)

	// respond to ad request with the winner, send win notification to winner
	c.String(http.StatusOK, winner.BidUrl)
	_, _ = sendWinNotif(conns[address], &pb.WinAlert{AuctionID: auctionID, Price: price}, timeout)
}

// getWinnerAndPrice returns the winning bid and the price to pay according to the price scaling used
func getWinnerAndPrice(bids map[string]*pb.BidResp) (address string, bid *pb.BidResp) {
	price := uint64(0)
	for dsp, resp := range bids {
		if bidAmount := resp.GetBidAmount(); bidAmount > price {
			price = bidAmount
			address = dsp
			bid = resp
		}
	}
	return
}
