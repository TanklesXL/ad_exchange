package main

import (
	"context"
	"sync"
	"time"

	pb "gitlab.com/tanklesxl/ad_exchange/auction"

	"google.golang.org/grpc"
)

// openConns opens the connections with each DSP and sets the given options.
func openConns(dsps []string, options ...grpc.DialOption) map[string]*grpc.ClientConn {
	conns := make(map[string]*grpc.ClientConn)

	for _, dsp := range dsps {
		conn, _ := grpc.Dial(dsp, options...)
		conns[dsp] = conn
	}
	return conns
}

// closeConns closes all provided connections.
func closeConns(conns map[string]*grpc.ClientConn) {
	for _, conn := range conns {
		conn.Close()
	}
}

type responseTuple struct {
	dsp  string
	resp *pb.BidResp
}

// broadcastBidRequest sends out all bid requests concurrently, returns the channel with the responses.
func broadcastBidRequest(conns map[string]*grpc.ClientConn, req *pb.BidReq, timeout time.Duration) map[string]*pb.BidResp {
	bids := make(chan responseTuple, len(conns))
	wg := &sync.WaitGroup{}
	for _, conn := range conns {
		wg.Add(1)
		go collectBid(conn, req, timeout, wg, bids)
	}
	wg.Wait()
	close(bids)
	bidMap := make(map[string]*pb.BidResp)
	for resp := range bids {
		bidMap[resp.dsp] = resp.resp
	}
	return bidMap
}

func collectBid(conn *grpc.ClientConn, req *pb.BidReq, timeout time.Duration, wg *sync.WaitGroup, bids chan<- responseTuple) {
	defer wg.Done()
	resp, err := sendBidRequest(conn, req, timeout)
	if err != nil || resp.GetBidAmount() == 0 || resp.GetBidUrl() == "" {
		return
	}
	bids <- responseTuple{dsp: conn.Target(), resp: resp}
}

// sendBidRequest sends a single bid request and puts its response on the channel.
func sendBidRequest(conn *grpc.ClientConn, req *pb.BidReq, timeout time.Duration) (*pb.BidResp, error) {
	c := pb.NewDSPClient(conn)
	ctx, cancel := context.WithTimeout(context.Background(), timeout)
	defer cancel()

	return c.Bid(ctx, req)
}

// sendWinNotif notifies the dsp server of a winning bid.
func sendWinNotif(conn *grpc.ClientConn, notif *pb.WinAlert, timeout time.Duration) (*pb.Empty, error) {
	c := pb.NewDSPClient(conn)
	ctx, cancel := context.WithTimeout(context.Background(), timeout)
	defer cancel()

	return c.NotifyWin(ctx, notif)
}
