package main

import (
	"flag"
	"log"
	"time"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"github.com/spf13/viper"

	"google.golang.org/grpc"
)

// readConfig reads  conf at path specified, fail otherwise.
func readConfig(file string) (*viper.Viper, error) {
	v := viper.New()
	v.SetConfigFile(file)
	if err := v.ReadInConfig(); err != nil {
		return nil, err
	}
	return v, nil
}

func main() {
	configPath := flag.String("conf", "conf.yml", "The path to the config gile")
	flag.Parse()

	v, err := readConfig(*configPath)
	if err != nil {
		log.Fatalf("failure reading config: %v", err)
	}

	dspList := v.GetStringSlice("dsps")
	log.Println(dspList)

	// open connections to all dsps
	conns := openConns(dspList, grpc.WithInsecure())
	defer closeConns(conns)

	// set up router with middleware and handlerfunc
	router := gin.Default()
	router.Use(
		func(c *gin.Context) { c.Set("CONNS", conns) },
		func(c *gin.Context) { c.Set("TIMEOUT", time.Duration(v.GetInt("timeout"))*time.Millisecond) },
		cors.Default(),
	)

	router.POST("/image", image)

	if err := router.Run(v.GetString("port")); err != nil {
		defer log.Panicf("failure running exchange: %v", err)
		return
	}
}
