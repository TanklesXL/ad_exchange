package main

import (
	"context"
	"flag"
	"log"
	"math/rand"
	"net"
	"time"

	"github.com/spf13/viper"
	pb "gitlab.com/tanklesxl/ad_exchange/auction"

	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type dspConfig struct {
	ads AdList
	pb.UnimplementedDSPServer
}

type AdList []AdDetails

type AdDetails struct {
	Type   string
	Width  uint64
	Height uint64
	URL    string
}

// Bid responds to an incoming bid request with the appropriate ad to fill the spot.
// An ad is returned if one is found to match the width, height and type specified.
func (dsp dspConfig) Bid(ctx context.Context, req *pb.BidReq) (*pb.BidResp, error) {
	log.Printf("Request received - auction: %s, width: %d, height: %d", req.GetAuctionID(), req.GetWidth(), req.GetHeight())

	// go through and see if any resources match the request
	var toBid AdDetails
	for _, ad := range dsp.ads {
		if ad.Width == req.GetWidth() &&
			ad.Height == req.GetHeight() &&
			ad.Type == isImageOrVideo(req.GetAdType()) {
			toBid = ad
			break
		}
	}
	if toBid.URL == "" {
		return nil, status.Errorf(codes.Unavailable, "No resource found matching the size requirements.")
	}

	rand.Seed(time.Now().UnixNano())
	bidPrice := rand.Uint64() % 100 //nolint

	log.Printf("Response sent - auction: %s, bid: %d", req.GetAuctionID(), bidPrice)
	return &pb.BidResp{BidAmount: bidPrice, BidUrl: toBid.URL}, nil
}

// isImageOrVideo returns string a based on the ad request type.
func isImageOrVideo(requestType pb.AdType) string {
	switch requestType {
	case pb.AdType_VIDEO:
		return "video"
	default:
		return "image"
	}
}

// NotifyWin is used to notify the server when its bid has won an auction.
func (dsp dspConfig) NotifyWin(ctx context.Context, alert *pb.WinAlert) (*pb.Empty, error) {
	log.Printf("Win - auction: %s, payment due: %d\n", alert.GetAuctionID(), alert.GetPrice())
	return &pb.Empty{}, nil
}

// readConfig reads  conf at path specified, fail otherwise.
func readConfig(file string) (*viper.Viper, error) {
	v := viper.New()
	v.SetConfigFile(file)
	if err := v.ReadInConfig(); err != nil {
		return nil, err
	}
	return v, nil
}

func main() {
	// parse command line flag config path
	configPath := flag.String("conf", "conf.yml", "The path to the config file")
	flag.Parse()

	v, err := readConfig(*configPath)
	if err != nil {
		log.Fatalf("failed to read config: %v", err)
	}

	var ads AdList
	if err := v.UnmarshalKey("ads", &ads); err != nil {
		log.Fatalf("failure getting ad data: %v", err)
	}

	// register the server to handle incoming grc requests
	grpcServer := grpc.NewServer()
	pb.RegisterDSPServer(grpcServer, dspConfig{ads: ads})

	// create a listener on the specified tcp port
	lis, err := net.Listen("tcp", v.GetString("port"))
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}

	// run the server
	if err := grpcServer.Serve(lis); err != nil {
		log.Fatalf("failed to serve grpc: %v", err)
	}
}
